﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

	[SerializeField] private GameObject _hitSparks;

	public void hit(RaycastHit hit, Bullet projectile) {
		GameObject sparks;
		sparks = Instantiate(_hitSparks, hit.point, Quaternion.LookRotation(hit.normal), transform);
	}

}

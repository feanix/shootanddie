﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

	[SerializeField] private GameObject _combat;
	[SerializeField] private GameObject _movements;
	[SerializeField] private GameObject _power;
	[SerializeField] private Text _powerField;
	[SerializeField] private Character _character;

	private bool _visible = true;

	private void updateVisibility(bool hasWeapon) {
		_combat.gameObject.SetActive(_visible && hasWeapon);
		_movements.gameObject.SetActive(_visible);
		_power.gameObject.SetActive(hasWeapon);
		updatePower(_character.power);
	}

	private void updatePower(float power) {
		_powerField.text = Math.Round(Math.Round(power)).ToString();
	}

	private void updateWeapon(Weapon weapon) {
		updateVisibility(weapon != null);
	}

	void Awake() {
		_character.onWeaponChange += updateWeapon;
		_character.onPowerChange += updatePower;
		updateVisibility(_character.hasWeapon);
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.H)) {
			_visible = !_visible;
			updateVisibility(_character.hasWeapon);
		}
	}

}

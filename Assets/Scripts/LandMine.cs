﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Explosion {

	[SerializeField] private GameObject _prefab;
	[SerializeField] private float _duration;
	[SerializeField] private GameObject _sound;

	public GameObject prefab {
		get { return _prefab; }
	}

	public float duration {
		get { return _duration; }
	}

	public GameObject sound {
		get { return _sound; }
	}

}

[RequireComponent(typeof(Collider))]
public class LandMine : MonoBehaviour {

	[SerializeField] private Explosion _explosion;

	void OnTriggerEnter(Collider collider) {
		Character player;
		player = collider.gameObject.GetComponent<Character>();
		if (player != null)
			explode(player);
	}

	public void explode(Character player = null) {
		GameObject explosion;
		explosion = Instantiate(_explosion.prefab, transform.position, transform.rotation, transform);

		Action removeExplosion = () => {
			Destroy(explosion);
		};

		StartCoroutine(delayAction(_explosion.duration, removeExplosion));

		if (player != null) {
			Vector3 direction;
			direction = player.transform.position - transform.position;
			direction.Normalize();
			direction.y = 0.7f;
			direction *= 7;
			player.thrust(transform, direction, ForceMode.Impulse);
		}
	}

	IEnumerator delayAction(float time, Action action) {
		yield return new WaitForSeconds(time);
		action.Invoke();
	}

	void Start() {

	}

    void Update() {
        
    }

}

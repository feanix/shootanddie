﻿using System;
using UnityEngine;

[Serializable]
public struct Triangle3 {

	[SerializeField] public Vector3 point1;
	[SerializeField] public Vector3 point2;
	[SerializeField] public Vector3 point3;

	public void normalize(float multiplier) {
		point1 = point1.normalized * multiplier;
		point2 = point2.normalized * multiplier;
		point3 = point3.normalized * multiplier;
	}

}

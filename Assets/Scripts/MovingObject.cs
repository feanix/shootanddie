﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class MovingObject : MonoBehaviour {

	[SerializeField] private float _X = 0f;
	[SerializeField] private float _Y = 0f;
	[SerializeField] private float _Z = 0f;
	[SerializeField] private float _speed = 5f;
	[SerializeField] private bool _bothWays = true;
	[SerializeField] private bool _rotation = false;
	[SerializeField][Range(0f, 1f)] private float _offset = 0.5f;
	[SerializeField] private Ease _ease = Ease.InOutQuad;

	private Vector3 _startCoords;
	private Vector3 _startPoint;
	private Vector3 _endPoint;
	private Vector3 _delta;
	private bool _firstIteration = true;
	private float _time;

	void Awake() {
		_delta = new Vector3(_X, _Y, _Z);
		if (!_rotation)
			_startCoords = transform.localPosition;
		else
			_startCoords = transform.localRotation.eulerAngles;
		if (!_bothWays) {
			_startPoint = _startCoords;
		} else {
			_startPoint = _startCoords - _delta;
		}
		_endPoint = _startCoords + _delta;
		_time = (_endPoint - _startPoint).magnitude / _speed;
		if (!_rotation)
			transform.localPosition = _startPoint;
		else
			transform.localRotation = Quaternion.Euler(_startPoint);
		move();
	}

	private void move() {
		Tweener tween;
		if (!_rotation) {
			tween = transform.DOLocalMove(_endPoint, _time);
		} else {
			tween = transform.DOLocalRotate(_delta, _time, RotateMode.WorldAxisAdd);
		}
		tween.SetEase(_ease);
		if (!_rotation && _bothWays)
			tween.onComplete = moveBack;
		else
			tween.onComplete = move;
		if (_firstIteration) {
			_firstIteration = false;
			tween.fullPosition = _time * _offset;
		}
	}

	private void moveBack() {
		Tweener tween;
		if (!_rotation)
			tween = transform.DOLocalMove(_startPoint, _time);
		else
			tween = transform.DOLocalRotate(-_delta, _time, RotateMode.WorldAxisAdd);
		tween.SetEase(_ease);
		tween.onComplete = move;
	}

}

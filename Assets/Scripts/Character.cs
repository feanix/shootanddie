﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour {

	private const float ROTATION_SPEED = 6f;
	private const float MOUSE_SPEED = 15f;
	private const float WALK_SPEED = 2f;
	private const float RUN_SPEED = 10f;
	private const float ACCELERATION = 0.1f;
	private const float DECELERATION = 0.3f;

	private const float JUMP_FORCE = 500f;

	public delegate void WeaponChangeHanler(Weapon weapon);
	public delegate void PowerChangeHanler(float power);

	public event WeaponChangeHanler onWeaponChange;
	public event PowerChangeHanler onPowerChange;

	[SerializeField] private Transform _gunMountingPoint;
	[SerializeField] private Animator _animator;
	[SerializeField] private Rigidbody _rigidBody;
	[SerializeField] private Transform _cameraContainer;
	[SerializeField] private Transform _playerModel;

	[SerializeField] private Camera _mainCamera;
	[SerializeField] private Camera _aimCamera;

	private Weapon _weapon;
	private Vector2 _moveDirection = Vector2.zero;
	private Vector2 _lastDirection = Vector2.zero;
	private Vector3 _lastMove = Vector3.zero;
	private Vector2 _rotation = Vector2.zero;
	private float _currentSpeed = 0;
	private float _lastTime = 0;
	private bool _running = false;
	private bool _knockDown = false;
	private bool _aim = false;

	public float power {
		get {
			if (hasWeapon)
				return _weapon.power;
			return 0; 
		}
		set {
			if (!hasWeapon)
				return;
			_weapon.power = value;
			if (onPowerChange != null)
				onPowerChange(power);
		}
	}

	void OnTriggerEnter(Collider collider) {
		Weapon weapon;
		weapon = collider.gameObject.GetComponent<Weapon>();
		if (weapon != null)
			arm(weapon);
	}

	private bool knockDown {
		get { return _knockDown; }
		set {
			_knockDown = value;
		}
	}

	private void arm(Weapon weapon) {
		if ((weapon != null) && knockDown)
			return;

		_weapon = weapon;
		aim = aim && (_weapon != null);

		if (_weapon != null) {
			_weapon.disablePhysics();
			_weapon.transform.parent = _gunMountingPoint;
			_weapon.transform.localPosition = Vector3.zero;
			_weapon.transform.localRotation = Quaternion.Euler(90, 0, 0);
		}

		_animator.SetBool("Rifle", _weapon != null);
		if (onWeaponChange != null)
			onWeaponChange(weapon);
	}

	public bool hasWeapon {
		get { return _weapon != null; }
	}

	void Update() {
		Vector2 scroll;
		_moveDirection = Vector2.zero;
		_running = false;
		aim = Input.GetMouseButton(1);
		if (!knockDown) {
			if (!aim) {
				_moveDirection.x = Input.GetAxis("Horizontal");
				_moveDirection.y = Input.GetAxis("Vertical");
				_moveDirection.Normalize();
				_running = Input.GetKey(KeyCode.LeftShift);
			}
			_rotation.x = Input.GetAxis("Mouse X");
			_rotation.y = Input.GetAxis("Mouse Y");
			scroll = Input.mouseScrollDelta;
			if (scroll.x != 0)
				power += scroll.x * 10;
			if (scroll.y != 0)
				power += scroll.y * 10;
		} else {
			_rotation = Vector2.zero;
		}
		/*if (Input.GetKeyDown(KeyCode.Space)) {
			jump();
		}*/
		if (aim && Input.GetMouseButtonDown(0)) {
			_weapon.fire(_aimCamera.transform);
		}
		animate();
		updateSpeed();
	}

	public void thrust(Transform source, Vector3 force, ForceMode forceMode) {
		_rigidBody.AddForce(force, forceMode);
		knockDown = true;

		Action recover = () => { knockDown = false; };

		StartCoroutine(delayAction(2f, recover));
		_animator.SetTrigger("Explosion");

		if (_weapon != null) {
			Vector3 globalTrajectory;
			Vector3 gunTrajectory = Vector3.zero;
			gunTrajectory.z = UnityEngine.Random.Range(0, 100);
			gunTrajectory.x = UnityEngine.Random.Range(-100, 100);
			gunTrajectory.Normalize();
			gunTrajectory.y = 0.5f;
			gunTrajectory *= 500;
			globalTrajectory = _playerModel.transform.TransformDirection(gunTrajectory);
			_weapon.transform.SetParent(null);
			_weapon.enablePhysics();
			_weapon.thrust(globalTrajectory);
			arm(null);
		}
	}

	IEnumerator delayAction(float time, Action action) {
		yield return new WaitForSeconds(time);
		action.Invoke();
	}

	private void jump() {
		Vector3 shift = _lastMove * 5000f;
		Vector3 direction = new Vector3(shift.x, JUMP_FORCE, shift.z);
		_rigidBody.AddForce(direction, ForceMode.Force);
	}

	private void animate() {
		if (_moveDirection.magnitude > 0) {
			if (_running) {
				_animator.ResetTrigger("Walk");
				_animator.ResetTrigger("Stop");
				_animator.SetTrigger("Run");
			} else {
				_animator.ResetTrigger("Run");
				_animator.ResetTrigger("Stop");
				_animator.SetTrigger("Walk");
			}
		} else {
			_animator.ResetTrigger("Run");
			_animator.ResetTrigger("Walk");
			_animator.SetTrigger("Stop");
		}
	}

	private void updateSpeed() {
		float deltaTime;
		float deltaSpeed;
		float maxSpeed = 0;
		Vector2 movement;
		Vector3 fullMovement;
		Quaternion cameraDirection;
		deltaTime = Time.time - _lastTime;
		_lastTime = Time.time;
		if (_moveDirection.magnitude == 0) {
			maxSpeed = 0;
		} else {
			cameraDirection = Quaternion.LookRotation(new Vector3(_moveDirection.x, 0f, _moveDirection.y)) * _mainCamera.transform.rotation;
			fullMovement = cameraDirection.eulerAngles;
			_moveDirection = new Vector2((float)Math.Sin(fullMovement.y*Math.PI/180f), (float)Math.Cos(fullMovement.y*Math.PI/180f));
			_moveDirection.Normalize();
			_lastDirection = _moveDirection;
			maxSpeed = _running ? RUN_SPEED : WALK_SPEED;
			maxSpeed /= 100;
		}
		if (_currentSpeed > maxSpeed) {
			deltaSpeed = DECELERATION * deltaTime;
			_currentSpeed -= deltaSpeed;
		} else {
			deltaSpeed = ACCELERATION * deltaTime;
			_currentSpeed += deltaSpeed;
			if (_currentSpeed > maxSpeed)
				_currentSpeed = maxSpeed;
		}

		if (_currentSpeed < 0)
			_currentSpeed = 0;
		movement = _lastDirection * _currentSpeed;
		move(movement);

		//if ((_rotation.magnitude != 0) || aim) {
			_rotation *= MOUSE_SPEED / 10;
			rotate(_rotation);
		//}
	}

	private void rotate(Vector2 rotation) {
		Vector3 cameraRotation;
		Vector3 playerRotation;
		cameraRotation = _cameraContainer.localRotation.eulerAngles + new Vector3(-rotation.y, rotation.x, 0);
		if ((cameraRotation.x < 300) && (cameraRotation.x > 180))
			cameraRotation.x = 300;
		if ((cameraRotation.x > 45) && (cameraRotation.x <= 180))
			cameraRotation.x = 45;
		playerRotation = cameraRotation;
		_cameraContainer.transform.localRotation = Quaternion.Euler(cameraRotation);
		cameraRotation.x += 15;
		_aimCamera.transform.localRotation = Quaternion.Euler(cameraRotation);
		if (aim) {
			playerRotation.x = 0;
			playerRotation.z = 0;
			_playerModel.transform.rotation = Quaternion.Euler(playerRotation);
		}
	}

	private void move(Vector2 direction) {
		float sign;
		Vector3 modelRotation;
		Vector3 newModelRotation;
		Vector3 direction3 = new Vector3(direction.x, 0f, direction.y);
		Vector3 newPosition;
		_lastMove = transform.TransformDirection(direction3);
		newPosition = transform.position + _lastMove;
		_rigidBody.MovePosition(newPosition);

		if (direction.magnitude > 0) {
			modelRotation = _playerModel.localRotation.eulerAngles;
			newModelRotation = new Vector3(0, (float)(Math.Atan2(direction.x, direction.y)*180f/Math.PI), 0);
			if (newModelRotation.y < 0)
				newModelRotation.y += 360;
			if ((newModelRotation.y - modelRotation.y) > 180)
				newModelRotation.y -= 360;
			if ((newModelRotation.y - modelRotation.y) < -180)
				modelRotation.y -= 360;

			sign = (newModelRotation.y - modelRotation.y) / Math.Abs(newModelRotation.y - modelRotation.y);
			if (Math.Abs(newModelRotation.y - modelRotation.y) < 180) {
				if (Math.Abs(newModelRotation.y - modelRotation.y) > ROTATION_SPEED)
					newModelRotation.y = modelRotation.y + ROTATION_SPEED* sign;
			} else {
				if (Math.Abs(modelRotation.y - newModelRotation.y) > ROTATION_SPEED)
					newModelRotation.y = modelRotation.y - ROTATION_SPEED* sign;
			}

			_playerModel.localRotation = Quaternion.Euler(newModelRotation);
		}
	}

	private bool aim {
		get { return _aim; }
		set {
			_aim = value && (_weapon != null);
			_aimCamera.enabled = _aim;
			_mainCamera.enabled = !_aim;
		}
	}

	void Awake() {
		aim = false;
	}
}

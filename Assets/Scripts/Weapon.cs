﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Weapon : MonoBehaviour {

	[SerializeField] private Bullet _bullet;

	private Rigidbody _rigidBody;
	private Collider _trigger;
	private float _power = Bullet.SPEED;

	public float power {
		get { return _power; }
		set {
			if (value > 400)
				value = 400;
			else if (value < 10)
				value = 10;
			_power = value;
		}
	}

	public Bullet fire(Transform startPoint) {
		Bullet result;
		Vector3 position = startPoint.position;
		position.y -= 0.2f;
		result = Instantiate(_bullet, position, startPoint.rotation);
		result.trace = true;
		result.fire(startPoint.rotation.eulerAngles.y, startPoint.rotation.eulerAngles.x, _power);
		return result;
	}

	void Awake() {
		_rigidBody = gameObject.GetComponent<Rigidbody>();
		_trigger = gameObject.GetComponent<Collider>();
	}

	public void thrust(Vector3 direction) {
		_rigidBody.AddForce(direction, ForceMode.Force);
    }

	public void disablePhysics() {
		_rigidBody.detectCollisions = false;
		_rigidBody.isKinematic = true;
	}

	public void enablePhysics() {
		_rigidBody.detectCollisions = true;
		_rigidBody.isKinematic = false;
	}

}

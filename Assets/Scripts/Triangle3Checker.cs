﻿using UnityEngine;

public class Triangle3Checker {

	private Triangle3 _triangle;

	private Vector3 _normal1;
	private Vector3 _normal2;
	private Vector3 _normal3;

	public Triangle3Checker(Triangle3 triangle) {
		_triangle = triangle;
		_normal1 = Vector3.Cross(_triangle.point1, _triangle.point2);
		_normal2 = Vector3.Cross(_triangle.point2, _triangle.point3);
		_normal3 = Vector3.Cross(_triangle.point3, _triangle.point1);
	}

	public bool checkPoint(Vector3 point) {
		bool result = false;

		float dot1 = Vector3.Dot(_normal1, point);
		float dot2 = Vector3.Dot(_normal2, point);
		float dot3 = Vector3.Dot(_normal3, point);

		/*if ((dot1 <= 0) && (dot2 <= 0) && (dot3 <= 0))
			result = true;*/
		if ((dot1 >= 0) && (dot2 >= 0) && (dot3 >= 0))
			result = true;

		return result;
	}

}

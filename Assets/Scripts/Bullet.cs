﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Rendering;
using Debug = UnityEngine.Debug;

public class Bullet : MonoBehaviour {

	public const float SPEED = 200f; //Units per second
	public const float DRAG = 20f; //Air traction coefficient
	public const float GRAVITY = 9.81f; //Gravitational constant

	private const uint FPS = 40;
	private const uint SKIP = 1;

	private const float LIFESPAN = 20f; //After that time the bullet will be destroyed
	private const float ANGLE = 0f;

	[SerializeField] private GameObject _hitEffect;

	public bool trace = false;

	private double _fps = 0f;
	private double _deltaTime;

	private float _speed;
	private float _drag;
	private float _gravity;
	private float _angle;
	private float _rotation;

	private float _speedX;
	private float _speedY;
	private bool _fired = false;
	private float _rotationX;
	private float _rotationZ;
	private Vector3 _startPoint;
	private double _startTime;
	private double _tick;

	private LineRenderer _line;

	public void fire(float rotation) {
		fire(rotation, ANGLE);
	}

	public void fire(float rotation, float angle) {
		fire(rotation, angle, SPEED);
	}

	public void fire(float rotation, float angle, float speed) {
		fire(rotation, angle, speed, DRAG, GRAVITY);
	}

	private Stopwatch _watch = new Stopwatch();
	private double getTime() {
		double result;
		//result =  Time.time;
		result = ((double)_watch.ElapsedMilliseconds)/1000d;
		return result;
	}

	private void startTimer() {
		_watch = Stopwatch.StartNew();
		_startTime = getTime();
	}

	public void fire(float rotation, float angle, float speed, float drag, float gravity) {
		_fired = true;
		startTimer();
		if (_fps <= 1)
			fps = FPS;
		_tick = 1;
		_rotation = (rotation + 180f) * (float)Math.PI / 180f;
		_rotationZ = (float)Math.Cos(_rotation);
		_rotationX = (float)Math.Sin(_rotation);
		_speed = speed;
		_angle = (angle + 180f) * (float)Math.PI / 180f;
		_drag = drag*_speed*_speed/80000f;
		_gravity = gravity/2f;
		_speedX = (float)Math.Cos(_angle) * _speed;
		_speedY = (float)Math.Sin(_angle) * _speed;
		_startPoint = transform.position;
		if (trace) {
			_line = new GameObject("BulletTrace").AddComponent<LineRenderer>();
			_line.shadowCastingMode = ShadowCastingMode.Off;
			_line.receiveShadows = false;
			_line.useWorldSpace = true;
			_line.startWidth = 0.05f;
			_line.endWidth = 0.05f;
			_line.positionCount = 1;
			_line.SetPosition(0, transform.position);
		} else {
			_line = null;
		}
	}

	public uint fps {
		set {
			_fps = (float)value;
			_deltaTime = 1f/_fps;
		}
	}

	private int _count = 0;
	void Update() {
		if (!_fired)
			return;

		++_count;
		if (_count % SKIP != 0)
			return;

		double time = 0;
		double currentTime;

		currentTime = getTime() - _startTime;

		Vector3 currentPosition;
		currentPosition = transform.position;

		_tick = Math.Floor(_tick) + 1d;
		do {
			if (_fps > 1)
				time = _tick * _deltaTime;
			else
				time = currentTime;
			if (time > currentTime) {
				time = currentTime;
				_tick = currentTime / _deltaTime;
			} else {
				++_tick;
			}

			if (time > LIFESPAN) {
				Destroy(gameObject);
				return;
			}

			if (checkTrajectory(ref currentPosition, time)) {
				Destroy(gameObject);
				return;
			}
		} while (currentTime > time);

		if (currentPosition.y < -10f)
			Destroy(gameObject);
		else {
			transform.position = currentPosition;
		}
	}

	private bool checkTrajectory(ref Vector3 position, double time) {
		float distance;
		Vector3 direction;
		RaycastHit hit;
		Vector3 newPosition;
		newPosition = getPosition(predict(time));

		if (_line != null) {
			++ _line.positionCount;
			_line.SetPosition(_line.positionCount - 1, newPosition);
		}

		direction = newPosition - position;
		distance = direction.magnitude;
		direction.Normalize();
		Ray ray = new Ray(position, direction);
		position = newPosition;
		if (Physics.Raycast(ray, out hit, distance)) {
			if (checkCollision(hit)) {

				return true;
			}
		}
		return false;
	}

	private bool checkCollision(RaycastHit hit) {
		if (hit.collider.tag == "Environment") {
			GameObject sparks;
			sparks = Instantiate(_hitEffect, hit.point, Quaternion.LookRotation(hit.normal), null);
			Destroy(gameObject);
			return true;
		}
		SphereTarget sphereTarget = hit.collider.gameObject.GetComponent<SphereTarget>();
		if (sphereTarget != null) {
			bool hitResult;
			hitResult = sphereTarget.checkCollision(hit.point);
			sphereTarget.hit(hitResult, hit);
			if (hitResult)
				Debug.Log("Hit");
			else
				Debug.Log("Miss");
			Destroy(gameObject);
			return true;
		}
		Target target = hit.collider.gameObject.GetComponent<Target>();
		if (target != null) {
			target.hit(hit, this);
			Destroy(gameObject);
			return true;
		}
		LandMine mine = hit.collider.gameObject.GetComponent<LandMine>();
		if (mine != null) {
			mine.explode();
			Destroy(gameObject);
			return true;
		}

		return false;
	}

	private Vector2 predict(double time) { //time in seconds
		Vector2 result;
		double timeSQR;
		timeSQR = time * time;
		result = Vector2.zero;
		result.x = (float)(_speedX * time - _drag * (float)timeSQR);
		result.y = (float)(_speedY * time - _gravity * timeSQR);
		return result;
	}

	private Vector3 getPosition(Vector2 prediction) {
		Vector3 result;
		result = new Vector3(_rotationX * prediction.x, prediction.y, _rotationZ * prediction.x);
		return result + _startPoint;
	}

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;

public class SphereTarget : MonoBehaviour {

	[SerializeField] public List<Triangle3> _triangles = new List<Triangle3>();
	[SerializeField] private float _diameter = 1f;
	[SerializeField] private float _linePrecision = 20;

	[SerializeField] private GameObject _missSparksPrefab;
	[SerializeField] private GameObject _hitSparksPrefab;

	[SerializeField] private Renderer _sphere;

	private List<LineRenderer> _lines = new List<LineRenderer>();
	private List<Triangle3Checker> _checkers = new List<Triangle3Checker>();

	private Color _sphereColor;

	void Awake() {
		LineRenderer line;
		Triangle3Checker checker;

		_sphereColor = _sphere.material.color;

		line = gameObject.GetComponent<LineRenderer>();
		if (line != null)
			Destroy(line);

		foreach (Triangle3 triangle in _triangles) {
			triangle.normalize(_diameter);
			checker = new Triangle3Checker(triangle);
			_checkers.Add(checker);

			line = draw(triangle);
			_lines.Add(line);
		}
	}

	public void hit(bool success, RaycastHit hit) {
		GameObject sparks;
		GameObject prefab = success ? _hitSparksPrefab : _missSparksPrefab;
		if (prefab != null)
			sparks = Instantiate(prefab, hit.point, Quaternion.LookRotation(hit.normal), transform);
		if (success) {
			_sphere.material.DOKill();
			var tween = _sphere.material.DOColor(new Color(255f, 0f, 0f), 0.2f);
			tween.onComplete = () => {
				_sphere.material.DOColor(_sphereColor, 0.2f);
			};
		}
	}

	private LineRenderer draw(Triangle3 triangle) {
		GameObject lineObject = new GameObject("lineObject");
		lineObject.transform.SetParent(transform, false);

		LineRenderer result = lineObject.AddComponent<LineRenderer>();
		result.numCornerVertices = 3;
		result.numCapVertices = 3;
		result.useWorldSpace = false;
		result.shadowCastingMode = ShadowCastingMode.Off;
		result.receiveShadows = false;
		result.startWidth = 0.1f;
		result.endWidth = 0.1f;
		result.positionCount = 0;

		drawSide(triangle.point1, triangle.point2, result);
		drawSide(triangle.point2, triangle.point3, result);
		drawSide(triangle.point3, triangle.point1, result);

		return result;
	}

	private void drawSide(Vector3 poin1, Vector3 point2, LineRenderer line) {
		Vector3 side;
		float length;
		int slicesNumber;
		Vector3 delta;
		Vector3 newPoint;
		side = point2 - poin1;
		length = side.magnitude;
		slicesNumber = (int)Math.Ceiling(length / _diameter * _linePrecision);
		delta = side / (float)slicesNumber;
		for (int i = 0; i < slicesNumber; i++) {
			line.positionCount += 1;
			newPoint = (poin1 + delta * i).normalized * _diameter;
			line.SetPosition(line.positionCount - 1, newPoint);
		}
	}

	public bool checkCollision(Vector3 point) {
		point = transform.InverseTransformPoint(point);
		foreach (Triangle3Checker checker in _checkers) {
			if (checker.checkPoint(point))
				return true;
		}
		return false;
	}

}

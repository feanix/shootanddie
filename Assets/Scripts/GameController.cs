﻿using UnityEngine;

public class GameController : MonoBehaviour {

    void Awake() {
		QualitySettings.vSyncCount = 0;
		Cursor.lockState = CursorLockMode.Locked;
		Physics.IgnoreLayerCollision(9, 10);
		Physics.IgnoreLayerCollision(9, 11);
		Physics.IgnoreLayerCollision(10, 11);
	}

	void Update() {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
			#else
				Application.Quit();
			#endif
		}
		if (Input.GetKey(KeyCode.LeftControl)) {
			Application.targetFrameRate = 2;
		} else {
			Application.targetFrameRate = -1;
		}
	}

}
